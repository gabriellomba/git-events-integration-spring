package com.hackerrank.github.service;

import java.util.Set;

import com.hackerrank.github.model.Event;

public interface EventService {

	public void saveEvent(Event event);

	public Set<Event> getEvents();

	public Set<Event> getEventsByActor(Long actorId);

	public void deleteAll();
}
