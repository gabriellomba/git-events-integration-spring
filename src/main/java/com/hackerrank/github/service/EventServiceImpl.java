package com.hackerrank.github.service;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackerrank.github.error.ActorNotFoundException;
import com.hackerrank.github.error.EventAlreadyExistsException;
import com.hackerrank.github.model.Event;
import com.hackerrank.github.repository.ActorRepository;
import com.hackerrank.github.repository.EventRepository;
import com.hackerrank.github.repository.RepoRepository;

@Service
public class EventServiceImpl implements EventService {

	@Autowired
	private EventRepository eventRepository;

	@Autowired
	private ActorRepository actorRepository;

	@Autowired
	private RepoRepository repoRepository;

	@Override
	@Transactional
	public void saveEvent(Event event) {
		if (eventRepository.exists(event.getId())) {
			throw new EventAlreadyExistsException();
		}

		actorRepository.save(event.getActor());
		repoRepository.save(event.getRepo());
		eventRepository.save(event);
	}

	@Override
	public Set<Event> getEvents() {
		return eventRepository.findAllByOrderByIdAsc();
	}

	@Override
	public Set<Event> getEventsByActor(Long actorId) {
		if (!actorRepository.exists(actorId)) {
			throw new ActorNotFoundException();
		}

		return eventRepository.findByActorOrderByIdAsc(actorId);
	}

	@Override
	public void deleteAll() {
		eventRepository.deleteAll();
	}
}
