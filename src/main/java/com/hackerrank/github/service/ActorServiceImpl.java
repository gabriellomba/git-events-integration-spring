package com.hackerrank.github.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackerrank.github.error.ActorNotFoundException;
import com.hackerrank.github.error.InvalidActorUpdateException;
import com.hackerrank.github.model.Actor;
import com.hackerrank.github.model.Event;
import com.hackerrank.github.repository.ActorRepository;

@Service
public class ActorServiceImpl implements ActorService {

	@Autowired
	private ActorRepository actorRepository;

	@Override
	@Transactional
	public void updateActorAvatar(Actor actor) {
		Actor savedActor = actorRepository.findOne(actor.getId());

		if (savedActor == null) {
			throw new ActorNotFoundException();
		}

		if (!savedActor.getLogin().equals(actor.getLogin())) {
			throw new InvalidActorUpdateException();
		}

		actorRepository.save(actor);
	}

	@Override
	public List<Actor> findAllOrderedByNumberOfEvents() {
		List<Actor> actors = new ArrayList<>(actorRepository.findAllOrderedByEventTimestamp());

		Collections.sort(actors, (ac1, ac2) -> {
			List<Event> events1 = ac1.getEvents();
			List<Event> events2 = ac2.getEvents();

			if (events1.size() != events2.size()) {
				return events2.size() - events1.size();
			}

			if (!events1.isEmpty()) {
				LocalDateTime latest1 = events1.get(0).getCreatedAt();
				LocalDateTime latest2 = events2.get(0).getCreatedAt();

				if (!latest1.equals(latest2)) {
					return latest2.compareTo(latest1);
				}
			}

			return ac1.getLogin().compareTo(ac2.getLogin());
		});

		return actors;
	}

	private long getDateDiffInDays(LocalDateTime dt1, LocalDateTime dt2) {
		LocalDate d1 = dt1.toLocalDate();
		LocalDate d2 = dt2.toLocalDate();

		return Math.abs(d2.toEpochDay() - d1.toEpochDay());
	}

	private Map<Long, Integer> buildActorStreakMap(Set<Actor> actors) {
		Map<Long, Integer> actorMaxStreakMap = new HashMap<>();

		for (Actor actor : actors) {
			Long actorId = actor.getId();
			List<Event> events = actor.getEvents();
			Integer currentStreak = 1;
			Integer maxStreak = 1;
			LocalDateTime lastEvent = events.get(0).getCreatedAt();

			for (int i = 1; i < events.size(); ++i) {
				LocalDateTime currentEvent = events.get(i).getCreatedAt();

				long daysDiff = getDateDiffInDays(currentEvent, lastEvent);
				if (daysDiff == 1) {
					++currentStreak;
					maxStreak = Math.max(maxStreak, currentStreak);
				} else if (daysDiff != 0) {
					currentStreak = 1;
				}
			}

			actorMaxStreakMap.put(actorId, maxStreak);
		}

		return actorMaxStreakMap;
	}

	@Override
	public List<Actor> findAllOrderedByMaxStreak() {
		Set<Actor> actors = actorRepository.findAllOrderedByEventTimestamp();
		Map<Long, Integer> actorStreakMap = buildActorStreakMap(actors);

		List<Actor> sortedActors = new ArrayList<>(actors);

		Collections.sort(sortedActors, (ac1, ac2) -> {
			Integer streak1 = actorStreakMap.get(ac1.getId());
			Integer streak2 = actorStreakMap.get(ac2.getId());

			if (streak1 != streak2) {
				return streak2 - streak1;
			}

			List<Event> events1 = ac1.getEvents();
			List<Event> events2 = ac2.getEvents();
			if (!events1.isEmpty()) {
				LocalDateTime latest1 = events1.get(0).getCreatedAt();
				LocalDateTime latest2 = events2.get(0).getCreatedAt();

				if (!latest1.equals(latest2)) {
					return latest2.compareTo(latest1);
				}
			}

			return ac1.getLogin().compareTo(ac2.getLogin());
		});

		return sortedActors;
	}
}
