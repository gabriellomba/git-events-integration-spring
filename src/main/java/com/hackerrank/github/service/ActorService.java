package com.hackerrank.github.service;

import java.util.List;

import com.hackerrank.github.model.Actor;

public interface ActorService {
	
	public void updateActorAvatar(Actor actor);
	
	public List<Actor> findAllOrderedByNumberOfEvents();
	
	public List<Actor> findAllOrderedByMaxStreak();
}
