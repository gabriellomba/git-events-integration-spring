package com.hackerrank.github.error;

import org.springframework.http.HttpStatus;

public final class EventAlreadyExistsException extends AbstractHttpException {

	private static final long serialVersionUID = -9027211650029773852L;
	private static final String ERROR_MESSAGE =
			"There exists an event with the id provided.";

	public EventAlreadyExistsException() {
		super(HttpStatus.BAD_REQUEST, ERROR_MESSAGE);
	}
}
