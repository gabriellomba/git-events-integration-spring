package com.hackerrank.github.error;

import org.springframework.http.HttpStatus;

public final class ActorNotFoundException extends AbstractHttpException {

	private static final long serialVersionUID = 4083832976308045965L;
	private static final String ERROR_MESSAGE = "There does not exist an actor with the id provided.";

	public ActorNotFoundException() {
		super(HttpStatus.NOT_FOUND, ERROR_MESSAGE);
	}
}
