package com.hackerrank.github.error;

import org.springframework.http.HttpStatus;

public final class InvalidActorUpdateException extends AbstractHttpException {

	private static final long serialVersionUID = 4083832976308045965L;
	private static final String ERROR_MESSAGE = "Updating fields other than the avatar is disallowed.";

	public InvalidActorUpdateException() {
		super(HttpStatus.BAD_REQUEST, ERROR_MESSAGE);
	}
}
