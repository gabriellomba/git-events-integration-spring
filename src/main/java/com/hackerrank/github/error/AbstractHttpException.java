package com.hackerrank.github.error;

import org.springframework.http.HttpStatus;

public class AbstractHttpException extends RuntimeException {

	private static final long serialVersionUID = 7291771158431673661L;

	private final HttpStatus status;

	AbstractHttpException(HttpStatus status, String message) {
		super(message);
		this.status = status;
	}

	public HttpStatus getStatus() {
		return status;
	}
}
