package com.hackerrank.github.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hackerrank.github.model.Actor;
import com.hackerrank.github.service.ActorService;

@RestController
@RequestMapping("/actors")
public class ActorRestController {

	@Autowired
	private ActorService actorService;

	@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public void updateActorAvatar(@RequestBody Actor actor) {
		actorService.updateActorAvatar(actor);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Actor> getActorsOrderedByEventTimestamp() {
		return actorService.findAllOrderedByNumberOfEvents();
	}
	
	@RequestMapping(value = "/streak", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Actor> getActorsOrderedByMaxStreak() {
		return actorService.findAllOrderedByMaxStreak();
	}
}
