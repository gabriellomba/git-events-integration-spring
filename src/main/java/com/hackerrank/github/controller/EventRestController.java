package com.hackerrank.github.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hackerrank.github.model.Event;
import com.hackerrank.github.service.EventService;

@RestController
@RequestMapping("/events")
public class EventRestController {

	@Autowired
	private EventService eventService;

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public void createEvent(@RequestBody Event event) {
		eventService.saveEvent(event);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Set<Event> getEvents() {
		return eventService.getEvents();
	}

	@RequestMapping(value = "/actors/{actorId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Set<Event> getEvents(@PathVariable("actorId") Long actorId) {
		return eventService.getEventsByActor(actorId);
	}
}
