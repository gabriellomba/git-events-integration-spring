package com.hackerrank.github.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hackerrank.github.service.EventService;

@RestController
public class EraseEventsRestController {

	@Autowired
	private EventService eventService;

	@RequestMapping(value = "/erase", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public void eraseAllEvents() {
		eventService.deleteAll();
	}
}
