package com.hackerrank.github.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Actor implements Serializable {

	private static final long serialVersionUID = -7061305737588805724L;

	@Id
	private Long id;

	@Column(unique = true)
	private String login;

	@JsonProperty("avatar_url")
	private String avatar;

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "actor")
	private List<Event> events;

	public Long getId() {
		return id;
	}

	public String getLogin() {
		return login;
	}

	public String getAvatar() {
		return avatar;
	}

	public List<Event> getEvents() {
		return events;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		Actor other = (Actor) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}

		return true;
	}
}
