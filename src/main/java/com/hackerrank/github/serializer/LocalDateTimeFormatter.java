package com.hackerrank.github.serializer;

import java.time.format.DateTimeFormatter;

public class LocalDateTimeFormatter {

	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	private LocalDateTimeFormatter() {
	}

	static final DateTimeFormatter getDateTimeFormatter() {
		return DATE_TIME_FORMATTER;
	}
}
