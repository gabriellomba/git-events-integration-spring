package com.hackerrank.github.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hackerrank.github.model.Event;

@Repository
public interface EventRepository extends CrudRepository<Event, Long> {

	@Query("FROM Event ev JOIN FETCH ev.actor JOIN FETCH ev.repo ORDER BY ev.id")
	Set<Event> findAllByOrderByIdAsc();

	@Query("FROM Event ev JOIN FETCH ev.actor JOIN FETCH ev.repo WHERE ev.actor.id = ?1 ORDER BY ev.id")
	Set<Event> findByActorOrderByIdAsc(Long actorId);
}
