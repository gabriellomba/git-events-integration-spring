package com.hackerrank.github.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hackerrank.github.model.Actor;

@Repository
public interface ActorRepository extends CrudRepository<Actor, Long> {
	
	@Query("FROM Actor ac JOIN FETCH ac.events ev ORDER BY ev.createdAt DESC")
	public Set<Actor> findAllOrderedByEventTimestamp();

}
